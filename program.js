//-------------Suma-------------------------
//function sumaSinparametros() {
//    let a = 5, b = 7;
//    return a + b;
//}
//console.log("Resultado de la Suma":,sumaSinparametros());

//-------------Suma con Parametro------------
//function sumaConparametros(n1, n2) {
//    let a = 5, b = 7;
//   return n1 + n2;
//}
//console.log(sumaConparametros(12, 7));

///----------------Resta con Parametros --------------
//function restaConparametros(n1, n2) {
//console.log(n1)
//return n1 -n2;
//}
//console.log("Resultado de la Resta:",restaConparametros(12, 7));

///----------------Multiplicacion con Parametros --------------
//function multiplicacionConparametros(n1, n2) {

//return n1 * n2;
//}
//console.log("Resultado de la multiplicacion:",multiplicacionConparametros(12, 7));

///----------------division con Parametros --------------
//function divisionConparametros(n1, n2) {
//    return n1 / n2;
//}
//console.log("Resultado de la division:", divisionConparametros(12, 7).toFixed(2));

//------------------ Calculadora con elementos del html------------------
//document.querySelector("#sumbtn").setAttribute("onclick","suma()");
//document.querySelector("#restbtn").setAttribute("onclick","resta()");
//document.querySelector("#multbtn").setAttribute("onclick","multiplicacion()");
//document.querySelector("#divbtn").setAttribute("onclick","division()");

//function suma() {


//    let n1 = parseInt(document.querySelector("#n1").value);
//    let n2 = parseInt(document.querySelector("#n2").value);
//    console.log(n1, n2);
//    document.querySelector("#resultado").innerHTML = n1 + n2;
//}




//function resta() {


//    let n1 = parseInt(document.querySelector("#n1").value);
//    let n2 = parseInt(document.querySelector("#n2").value);
//    console.log(n1, n2);
//    document.querySelector("#resultado").innerHTML = n1 - n2;
//}


//function multiplicacion() {


//    let n1 = parseInt(document.querySelector("#n1").value);
//    let n2 = parseInt(document.querySelector("#n2").value);
//    console.log(n1, n2);
//    document.querySelector("#resultado").innerHTML = n1 * n2;
//}



//function division() {


//    let n1 = parseInt(document.querySelector("#n1").value);
//    let n2 = parseInt(document.querySelector("#n2").value);
//    console.log(n1, n2);
//    document.querySelector("#resultado").innerHTML = n1 / n2;
//}

//--------------------- Ahora el codigo con objeto de datos{}----------------

//function datos() {
//    let misDatos = {
//       n1: parseInt(document.querySelector("#n1").value),
//        n2: parseInt(document.querySelector("#n2").value),
//       resultado: document.querySelector("#resultado")
//    };
//    return misDatos;
//}

//function suma() {
//    const objDatos = datos();
//    objDatos.resultado.innerHTML = objDatos.n1 + objDatos.n2;

//}


//function resta() {
//    const objDatos = datos();
//    objDatos.resultado.innerHTML = objDatos.n1 - objDatos.n2;

//}



//function multiplicacion() {
//    const objDatos = datos();
//    objDatos.resultado.innerHTML = objDatos.n1 * objDatos.n2;

//}



//function division() {
//    const objDatos = datos();
//   objDatos.resultado.innerHTML = (objDatos.n1 / objDatos.n2).toFixed(2);

//}

//function botonesFunciones() {
//   document.querySelector("#sumbtn").setAttribute("onclick", "suma()");
//    document.querySelector("#restbtn").setAttribute("onclick", "resta()");
//    document.querySelector("#multbtn").setAttribute("onclick", "multiplicacion()");
//    document.querySelector("#divbtn").setAttribute("onclick", "division()");
//}



//function main() {
//    botonesFunciones();

//}

//main();



//-------------- Con una sola funcion de calculo-----------------

function datos() {
    let misDatos = {
        n1: parseInt(document.querySelector("#n1").value),
        n2: parseInt(document.querySelector("#n2").value),
        resultado: document.querySelector("#resultado")
    };
    return misDatos;
}

function calcular(operacion) {

    const objDatos = datos();

    switch (operacion) {
        case 's':
            objDatos.resultado.innerHTML = objDatos.n1 + objDatos.n2;
            break;

        case 'r':
            objDatos.resultado.innerHTML = objDatos.n1 - objDatos.n2;
            break;
        case 'm':
            objDatos.resultado.innerHTML = objDatos.n1 * objDatos.n2;
            break;
        case 'd':
            objDatos.resultado.innerHTML = (objDatos.n1 / objDatos.n2).toFixed(2);
            break;

        default:
            break;

    }

}



function botonesFunciones() {
    document.querySelector("#sumbtn").setAttribute("onclick", "calcular('s')");
    document.querySelector("#restbtn").setAttribute("onclick", "calcular('r')");
    document.querySelector("#multbtn").setAttribute("onclick", "calcular('m')");
    document.querySelector("#divbtn").setAttribute("onclick", "calcular('d')");
}
function main() {
    botonesFunciones();
}

main();